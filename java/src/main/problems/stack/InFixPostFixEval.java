package main.problems.stack;

import main.BasicOperatorsManipulation;

import java.util.Stack;

/**
 * Created by jujhar on 11/1/17.
 */
public class InFixPostFixEval {

    public static int evalInFixExpression(String expression){
        Stack<Integer> operands = new Stack<>();
        Stack<Character> operators = new Stack<>();
        char [] exp = expression.toCharArray();
        for (char element:exp) {
            if(!BasicOperatorsManipulation.isOperator(element)){
                operands.push(Character.getNumericValue(element));
                if(operators.size()>0 && operands.size()>1){
                    int num2 = operands.pop().intValue();
                    int num1 = operands.pop().intValue();
                    char op = operators.pop().charValue();
                    operands.push(BasicOperatorsManipulation.evaluate(num1,num2,op));
                }
            }
            else {
                operators.push(element);
            }
        }
        return operands.pop();
    }
    public static int evalPostFixExpression(String expression){
        Stack<Integer> operands = new Stack<>();
        Stack<Character> operators = new Stack<>();
        char [] exp = expression.toCharArray();
        for (char element:exp) {
            if(BasicOperatorsManipulation.isOperator(element)){
                operators.push(element);
                if(operators.size()>0 && operands.size()>1){
                    int num2 = operands.pop();
                    int num1 = operands.pop();
                    char op = operators.pop();
                    operands.push(BasicOperatorsManipulation.evaluate(num1,num2,op));
                }
            }
            else {
                operands.push(Character.getNumericValue(element));
            }
        }
        return operands.pop();
    }
    public static String convertInFixIntoPostFixExpression(String expression){
        Stack<String> operands = new Stack<>();
        Stack<Character> operators = new Stack<>();
        char [] expressionArr = expression.toCharArray();

        for (char element: expressionArr) {
            if(!BasicOperatorsManipulation.isOperator(element) && !BasicOperatorsManipulation.isBracket(element)){
                operands.push(String.valueOf(element));
            }
            else if (BasicOperatorsManipulation.isOperator(element)){
                if(operators.size()>1){
                    if(BasicOperatorsManipulation.hasHigherPrecedency(element,operators.peek())){
                        operators.push(element);
                    }
                    else {
                        if(BasicOperatorsManipulation.isBracket(operators.peek())){
                            operators.push(element);
                        }
                        else {
                            String st2 = operands.pop();
                            String st1 = operands.pop();
                            char op = operators.pop();
                            String re = st1+st2+String.valueOf(op);
                            operands.push(re);
                            operators.push(element);
                        }
                    }
                }
                else {
                    operators.push(element);
                }
            }
            else if(BasicOperatorsManipulation.isOpeningBracket(element)){
                operators.push(element);
            }
            else {
                boolean flag = true;
                while (flag){
                    char op = operators.pop();
                    if (BasicOperatorsManipulation.isOpeningBracket(op)){
                        flag = false;
                    }
                    else {
                        String st2 = operands.pop();
                        String st1 = operands.pop();
                        String re = st1+st2+String.valueOf(op);
                        operands.push(re);
                        operators.push(element);
                    }
                }
            }
        }
        while (!operators.isEmpty()){
            String st2 = operands.pop();
            String st1 = operands.pop();
            char op = operators.pop();
            String re = st1+st2+String.valueOf(op);
            operands.push(re);
        }
        return operands.pop();
    }
    public static void main(String args[]){
        String inFixExpression = "+/*2322";
        String postFixExpression = "223*2/+";
        System.out.println(evalInFixExpression(inFixExpression));
        System.out.println(evalPostFixExpression(postFixExpression));
    }
}
