package main.problems.array;

import main.arrayutility.ArrayUtility;

import java.util.ArrayList;

/**
 * Created by jujhar on 10/1/17.
 */
public class AddBigTwoNumber {

    public static int[] add(String s1,String s2){
        int s1Len = s1.length();
        int s2Len = s2.length();
        int max = s1Len>s2Len?s1Len+1:s2Len+1;
        int result[] = new int[max];
        int i=s1Len-1,j=s2Len-1,k=max-1;
        int carry =0;
        while (i>=0 && j>=0 && k>0){
            int num1 = Character.getNumericValue(s1.charAt(i));
            int num2 = Character.getNumericValue(s2.charAt(j));
            int re = num1+num2+carry;
            if(re>9){
                carry = re/10;
                int remain = re%10;
                result[k]=remain;
                k--;
            }
            else{
                carry =0;
                result[k]=re;
                k--;
            }
            i--;j--;
        }
        if(i>=0 &&k>=0){
            while (k>=0 && i>=0){
                int num1 = Character.getNumericValue(s1.charAt(i));
                result[k]=num1;
                k--;
                i--;
            }
        }
        else{
            while (k>=0 && j>=0){
                int num = Character.getNumericValue(s1.charAt(j));
                result[k]=num;
                k--;
                j--;
            }
        }
        return result;
    }
    public static void main(String args[]){
        String s1 = "123";
        String s2 = "127";
        ArrayUtility.print(add(s1,s2));
    }
}
