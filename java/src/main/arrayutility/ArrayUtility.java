package main.arrayutility;

/**
 * Created by jujhar on 9/1/17.
 * Created to handle simple array operations like print array ,swipe numbers etc.
 */
public class ArrayUtility {
    public static void print(int A[]) {
        int length = A.length;
        for (int i = 0; i < length; i++) {
            System.out.print(A[i]);
        }
        System.out.println();
    }
    public static void merge(int left[],int right[],int A[]){
        int rightLen = right.length;
        int leftLen = left.length;
        int i = 0,j=0,k=0;
        while (i<rightLen && j<leftLen){
            if(left[j]<=right[i]){
                A[k] = left[j];
                j++;
                k++;
            }
            else {
                A[k] = right[i];
                i++;
                k++;
            }
        }
        if(i<rightLen){
            while (i<rightLen){
                A[k] = right[i];
                i++;
                k++;
            }
        }
        else {
            while (j<leftLen){
                A[k] = left[j];
                j++;
                k++;
            }
        }
    }

    public static void swipe(int A[], int i, int j) {
        int te = A[i];
        A[i] = A[j];
        A[j] = te;
    }

    public static int[] extract(int A[], int strIndex, int endIndex) {
        if(strIndex == endIndex){
            int singleElementArr[] = new int[1];
            singleElementArr[0] = A[strIndex];
            return singleElementArr;
        }
        int length = endIndex - strIndex;
        length++;
        int extracted[] = new int[length];
        int j = 0;
        for (int i = strIndex; i <= endIndex && j < length; i++) {
            extracted[j] = A[i];
            j++;
        }
        return extracted;
    }
}
