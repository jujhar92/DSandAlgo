package main.searching;

/**
 * Created by jujhar on 9/1/17.
 */
public class BinarySearch {
    public static void main(String args[]) {
        int A[] = {1,2,3,4,5,6,7};
        System.out.println(recursiveSearch(A,0,A.length-1,5));
        System.out.println(recursiveSearch(A,0,A.length-1,8));
        System.out.println(iterativeSearch(A,0,A.length-1,5));
        System.out.println(iterativeSearch(A,0,A.length-1,8));

    }
    public static int recursiveSearch(int A[],int str,int end,int a){
        if(str>end){
            return -1;
        }
        int mid = (end+str)/2;
        if(A[mid]== a){
            return mid;
        }
        if(a>A[mid]){
            return iterativeSearch(A,mid+1,end,a);
        }
        else {
            return iterativeSearch(A,str,mid-1,a);
        }
    }
    public static int iterativeSearch(int A[],int str,int end,int a){
        while(str<=end){
            int mid = str+(end-str)/2;
            if(A[mid] == a){
                return mid;
            }
            if(a>A[mid]){
                str = mid+1;
            }
            else {
                end = mid-1;
            }
        }
        return -1;
    }
}
