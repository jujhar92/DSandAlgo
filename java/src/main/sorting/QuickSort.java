package main.sorting;

import main.arrayutility.ArrayUtility;

/**
 * Created by jujhar on 9/1/17.
 */
public class QuickSort {
    public static void main(String args[]) {
        int A[] = {2,5,1,7,5,9,3};
        ArrayUtility.print(A);
        sort(A,0,A.length-1);
        ArrayUtility.print(A);
    }

    public static void sort(int A[],int str,int end) {
        if(str>=end){
            return;
        }
        int pIndex = partition(A,str,end);
        sort(A,str,pIndex-1);
        sort(A,pIndex+1,end);
    }

    public static int partition(int A[], int str, int end) {
        int pivot = A[end];
        int pIndex = str;
        while (str < end) {
            if(A[str]<=pivot){
                ArrayUtility.swipe(A,str,pIndex);
                pIndex++;
            }
            str++;
        }
        ArrayUtility.swipe(A,pIndex,end);
        return pIndex;
    }
}
