package main.sorting;

import main.StopWatch;
import main.arrayutility.ArrayUtility;

/**
 * Created by jujhar on 9/1/17.
 */
public class MergeSort {

    public static void sort(int A[]){
        StopWatch timer = new StopWatch();
        timer.start();
        int length = A.length;
        int mid = length/2;
        if(mid<1){
            return;
        }
        int left[] = ArrayUtility.extract(A,0,mid-1);
        int right[] = ArrayUtility.extract(A,mid,length-1);
        sort(left);
        sort(right);
        ArrayUtility.merge(left,right,A);
        timer.stop();
        //System.out.println("time taken to sort Array is : "+timer.elapsedTime()+"  nanoSeconds");
    }
    public static void main(String args[]){
        int A[] = {1,6,7,8,3,2};
        ArrayUtility.print(A);
        sort(A);
        ArrayUtility.print(A);

    }
}
