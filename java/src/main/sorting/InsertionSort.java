package main.sorting;

import main.StopWatch;
import main.arrayutility.ArrayUtility;

/**
 * Created by jujhar on 9/1/17.
 */
public class InsertionSort {

    public static void badSort(int A[]){
        StopWatch timer = new StopWatch();
        timer.start();
        int length = A.length;
        for (int i=1;i<length;i++){
            int j = i;
            while (j>0){
                if(A[j]<A[j-1]){
                    ArrayUtility.swipe(A,j-1,j);
                }
                j--;
            }
        }
        timer.stop();
        System.out.print("bad sort method took time : "+timer.elapsedTime() + "  nano seconds");
    }
    public static void sort(int A[]){
        StopWatch timer = new StopWatch();
        timer.start();
        int length = A.length;
        for (int i=1;i<length;i++){
            int holeValue = A[i];
            int hole = i;
            while (hole>0 && A[hole-1]>holeValue){
                A[hole] = A[hole-1];
                hole --;
            }
            A[hole] = holeValue;
        }
        timer.stop();
        System.out.print("sort method took time : "+timer.elapsedTime() + "  nano seconds");
    }

    public static void main(String args[]){
        int A[] ={1,8,4,3,2,9};
        ArrayUtility.print(A);
        sort(A);

        System.out.println("");
        ArrayUtility.print(A);
        System.out.println("");
        int B[] ={1,8,4,3,2,9};
        badSort(B);
    }
}
