package main;

/**
 * Created by jujhar on 9/1/17.
 */
public class StopWatch {
    private long startTime;
    private long stopTime;
    private long elapsedTime;
    public void start(){
        this.startTime = System.nanoTime();
    }
    public void stop(){
        this.stopTime = System.nanoTime();
    }
    public long elapsedTime(){
        this.elapsedTime = this.stopTime - this.startTime;
        return this.elapsedTime;
    }
}
