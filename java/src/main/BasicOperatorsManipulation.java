package main;

/**
 * Created by jujhar on 11/1/17.
 */
public class BasicOperatorsManipulation {
    public static int evaluate(int a , int b,char operator){
        switch (operator){
            case '+':
                return a+b;
            case '*':
                return a*b;
            case '/':
                return a/b;
            case '-':
                return a-b;
            default:
                return 0;
        }
    }
    public static boolean isOperator(char operator){
        switch (operator){
            case '+':
                return true;
            case '*':
                return true;
            case '/':
                return true;
            case '-':
                return true;
            default:
                    return false;
        }
    }
    public static boolean hasHigherPrecedency(char operator1 ,char operator2){
        if( (operator1 == '/' || operator1 == '*') && (operator2 == '/' || operator2 == '*')){
            return  false;
        }
        if( (operator1 == '+' || operator1 == '-') && (operator2 == '+' || operator2 == '-')){
            return  false;
        }
        if( (operator1 == '/' || operator1 == '*') && (operator2 == '+' || operator2 == '-')){
            return  true;
        }
        return false;
    }
    public static boolean isBracket(char op){
        if( op == '(' || op == ')' || op == '{' || op == '}' || op == '[' || op == ']'){
            return true;
        }
        return false;
    }
    public static boolean isMatchingBrackets(char op1 ,char op2){
        if ((op1 == '(' && op2 == ')') || (op1 == ')' && op2 == '(')){
            return true;
        }
        if ((op1 == '{' && op2 == '}') || (op1 == '}' && op2 == '{')){
            return true;
        }
        if ((op1 == '[' && op2 == ']') || (op1 == ']' && op2 == '[')){
            return true;
        }
        return false;
    }
    public static boolean isOpeningBracket(char op){
        if(op == '(' || op == '{' || op == '['){
            return true;
        }
        return false;
    }
}
